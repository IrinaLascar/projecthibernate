package org.example.dao;

import org.example.database.DatabaseConfig;
import org.example.entities.Address;
import org.example.entities.Person;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PersonDao {

    public Person createPerson(Person person) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.persist(person);

        transaction.commit();
        session.close();

        return person;
    }

    public Person readPerson(String cnp) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Person person = session.find(Person.class, cnp);
        session.close();
        return person;
    }
    public List<Person> readAllPerson() {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        // caută în entitate
        // Address este numele clasei (al entității)
        List<Person> persons = session.createQuery("select p from Person p", Person.class).getResultList();
        session.close();
        return persons;
    }

    public Person updatePerson(Person person) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(person);
        transaction.commit();
        session.close();

        return person;
    }
    public void removePerson(Person person) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(person);
        transaction.commit();
        session.close();
    }

    public List<Person> youngerThan(Integer year) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        List<Person> result = session.createQuery("select p from Person p where yearOfBirth > :yearParam", Person.class)
                .setParameter("yearParam", year)
                .getResultList();
        session.close();
        return result;
    }
    public List<Person> allFrom(String city) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        List<Person> personFrom = session.createQuery(
                        "select p from Person p where p.address.id.city = :cityParam", Person.class)
                .setParameter("cityParam", city)
                .getResultList();
        session.close();
        return personFrom;
    }
}
