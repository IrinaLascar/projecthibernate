package org.example;

import org.example.dao.AddressDao;
import org.example.dao.PersonDao;
import org.example.entities.Address;
import org.example.entities.AddressId;
import org.example.entities.BuildingType;
import org.example.entities.Person;

import java.util.HashMap;
import java.util.Map;


public class Main {
    private static AddressDao addressDao = new AddressDao();
    private static PersonDao personDao = new PersonDao();
    public static void main(String[] args) {
        AddressId addressId = new AddressId("Primaverii", 14, "Cluj-Napoca");
        AddressId addressId1 = new AddressId("1 decembrie", 2, "Iasi");
        AddressId addressId2 = new AddressId("Ion Creanga", 25, "Bacau");

        Address address = new Address(addressId, 150, BuildingType.PRIVATE_HOUSE);
        Address address1 = new Address(addressId1, 500, BuildingType.DUPLEX);
        Address address2 = new Address(addressId2, 270, BuildingType.OFFICE);

        addressDao.createAddress(address);
        addressDao.createAddress(address1);
        addressDao.createAddress(address2);



        Person firstPerson = new Person("1980101332255", "Ionescu", 1998, address);
        personDao.createPerson(firstPerson);

        Address a1 = addressDao.readAddress(addressId); //am salvat adresa intr-un obiect
        System.out.println(a1);
        System.out.println(personDao.readPerson("1980101332255"));

        Person ion = new Person("193040567842345", "Ion", 1993, address);
        Person vlad = new Person("177040567842345", "Vlad", 1977, address);
        Person mihai = new Person("186040567842345", "Mihai", 1986, address);
        Person dorin = new Person("168040567842345", "Dorin", 1968, address);
        Person maria = new Person("289121298983556", "Maria", 1989, address);
        Person ioana = new Person("299121298983556", "Ioana", 1999, address1);
        Person simona = new Person("278121298983556", "Simona", 1978, address);

        personDao.createPerson(ion);
        personDao.createPerson(vlad);
        personDao.createPerson(mihai);
        personDao.createPerson(dorin);
        personDao.createPerson(maria);
        personDao.createPerson(ioana);
        personDao.createPerson(simona);

     //   personDao.readAllPerson().forEach(p-> System.out.println(p));

       /* v1
        personDao.readAllPerson().forEach((p) -> {
            prettyDisplayOfPerson(p);
        }); */
// v2
        personDao.readAllPerson().forEach(p -> prettyDisplayOfPerson(p));

/* v3
        personDao.readAllPerson().forEach(Main::prettyDisplayOfPerson); */

        simona.setName("Irina");
        personDao.updatePerson(simona);

        ion.setAddress(address1);
        personDao.updatePerson(ion);

        vlad.setAddress(address2);
        personDao.updatePerson(vlad);


        personDao.removePerson(maria);
        System.out.println("***********");
        personDao.readAllPerson().forEach(p -> System.out.println(p));

        System.out.println("young");
        personDao.youngerThan(1990).forEach(System.out::println);

        System.out.println("-------------------");
        personDao.allFrom("Cluj-Napoca").forEach(System.out::println);

        Map<Integer, String> numbers = new HashMap<>();
        numbers.put(11, "unsprezece");
        numbers.put(100, "o sută");
        numbers.put(-2, "minus doi");

        numbers.forEach((k, v) -> {
            System.out.println("Numărul " + k + " se pronunță " + v);
        });

        numbers.forEach((k,v) -> prettyPrint(k,v));

        numbers.forEach(Main::prettyPrint);
    }
    static void prettyDisplayOfPerson(Person p) {
        System.out.println(
                p.getName() +
                        " născut în " + p.getYearOfBirth() +
                        " locuiește în " + p.getAddress().getId().getCity());
    }
    static void prettyPrint(Integer nr, String traducere) {
        System.out.println("+++ Numărul " + nr + " se pronunță " + traducere + " ++++");

    }

}