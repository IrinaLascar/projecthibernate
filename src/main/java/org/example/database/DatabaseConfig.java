package org.example.database;

import org.example.entities.Address;
import org.example.entities.Person;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DatabaseConfig {
    private static SessionFactory sessionFactory;

    private DatabaseConfig() {

    } // creare constructor gol de configurare baza de date

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = new Configuration()
                    .configure("hibernate.config.xml")
                    .addAnnotatedClass(Address.class)
                    .addAnnotatedClass(Person.class)

                    .buildSessionFactory();
        }
        return sessionFactory;

    }
}
